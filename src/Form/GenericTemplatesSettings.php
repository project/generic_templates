<?php
declare(strict_types=1);

namespace Drupal\generic_templates\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Theme\Registry;

/**
 * Configure Generic Templates settings for this site.
 */
class GenericTemplatesSettings extends ConfigFormBase
{
  protected EntityTypeManagerInterface $entityTypeManager;
  protected Registry $themeRegistry;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, Registry $theme_registry)
  {
    $this->entityTypeManager = $entity_type_manager;
    $this->themeRegistry = $theme_registry;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('theme.registry')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'generic_templates_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['generic_templates.settings'];
  }

  protected function getOptions(): array
  {
    $options = [];

    /// @var EntityTypeInterface $definition
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityType) {
        $options[$definition->id()] = $definition->getLabel();
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $config = $this->config('generic_templates.settings');

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => t('Debug'),
      '#description' => 'Highlight generic-templates for debugging',
      '#default_value' => $config->get('debug')
    ];

    $form['types'] = [
      '#type' => 'details',
      '#title' => t('Generic Types'),
      '#open' => TRUE,
      'types' => [
        '#type' => 'checkboxes',
        '#description' => t("Use a generic template with minimal CSS for selected Entities. Thats good for theming based on 'compositional CSS selectors'."),
        '#options' => $this->getOptions(),
        '#default_value' => $config->get('types')
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $selected = array_filter($form_state->getValue('types'));
    // @todo clear render cache tags for every changed entity_type
    // $changed = array_xor($config->set('types', $checked);

    $config = $this->config('generic_templates.settings');
    $config->set('types', $selected);
    $config->set('debug', $form_state->getValue('debug'));
    $config->save();

    parent::submitForm($form, $form_state);

    $this->themeRegistry->reset();
  }
}
