<?php
declare(strict_types=1);

namespace Drupal\generic_templates\EventSubscriber;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityViewEvent;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Theme\ThemeEvent;
use Drupal\core_event_dispatcher\Event\Theme\ThemeSuggestionsAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\core_event_dispatcher\ThemeHookEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Generic Templates event subscriber.
 */
class GenericTemplatesSubscriber implements EventSubscriberInterface
{
  private ImmutableConfig $settings;

  protected const PREFIX = '__generic';

  public function __construct(
    ConfigFactoryInterface $config_factory,
    private readonly ModuleExtensionList $moduleExtensionList
  ) {
    $this->settings = $config_factory->get('generic_templates.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      // @see https://www.drupal.org/docs/7/creating-custom-modules/howtos/using-template-files-in-your-own-module#drupal8
      ThemeHookEvents::THEME => 'theme',
      ThemeHookEvents::THEME_SUGGESTIONS_ALTER => 'themeSuggestionsAlter',

      // Add generic classes to entity attributes
      EntityHookEvents::ENTITY_VIEW => 'entityView',
      FormHookEvents::FORM_ALTER => 'formAlter'
    ];
  }

  /**
   * Add generic themes {entity_type}--generic to suggestions
   *
   * @param ThemeSuggestionsAlterEvent $event
   * @return void
   */
  public function themeSuggestionsAlter(ThemeSuggestionsAlterEvent $event): void
  {
    $hook = $event->getHook();

    if (in_array($hook, $this->settings->get('types') ?: [])) {
      // use the generic template lastly by adding it to beginning using array_unshift
      array_unshift($event->getSuggestions(), $hook . self::PREFIX);
    }
  }

  /**
   * Provide module defined templates for common entity_types
   *
   * @see generic_templates/templates folder
   *
   * @param ThemeEvent $event
   * @return void
   */
  public function theme(ThemeEvent $event): void
  {
    // Array of types for which generic_templates provides default templates
    // @todo retrieve this using File API
    $provided = ['user', 'node', 'media', 'comment', 'paragraph', 'group', 'group_relationship'];

    $path = $this->moduleExtensionList->getPath('generic_templates') . '/templates/entity';

    $types = array_intersect($this->settings->get('types') ?: [], $provided);
    foreach($types as $type) {
      $event->addNewTheme($type . self::PREFIX, ['path' => $path, 'base hook' => $type]);
    }
  }

  protected function classify(array &$attributes, ContentEntityInterface $entity, string $view_mode): void
  {
    $attributes = NestedArray::mergeDeep($attributes, [
      'class' => [
        Html::getClass($entity->getEntityTypeId()),
        Html::getClass($entity->bundle()),
        Html::getClass($view_mode),
      ]
    ]);

    if($this->settings->get('debug')) {
      $attributes['class'][] = 'generic-template';
    }
  }

  public function entityView(EntityViewEvent $event): void
  {
    $entity = $event->getEntity();

    if ($entity instanceof ContentEntityInterface && in_array($entity->getEntityTypeId(), $this->settings->get('types') ?: [])) {
      $build = &$event->getBuild();
      $build['#attributes'] ??= [];
      $this->classify($build['#attributes'], $entity, $event->getViewMode());
    }
  }

  public function formAlter(FormAlterEvent $event): void
  {
    $form_object = $event->getFormState()->getFormObject();

    if ($form_object instanceof ContentEntityForm) {
      $entity = $form_object->getEntity();

      if (in_array($entity->getEntityTypeId(), $this->settings->get('types') ?: [])) {
        // @todo 'full' is not correct... we may call this 'form'?? instead?
        $this->classify($event->getForm()['#attributes'], $entity, 'full');
      }
    }
  }
}
